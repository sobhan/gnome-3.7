# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/gnome-extra/gnome-clocks/gnome-clocks-0.1.5.ebuild,v 1.1 2012/12/27 07:11:25 tetromino Exp $

EAPI="5"
GCONF_DEBUG="no"

inherit gnome2

DESCRIPTION="note editor applications for GNOME"
HOMEPAGE="http://live.gnome.org/bijiben"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
	>=app-misc/tracker-0.14
	sys-devel/gettext
	>=x11-libs/gtk+-3.5.19:3
	dev-libs/libxml2
	>=dev-libs/glib-2.28:2
	media-libs/clutter-gtk:1.0
	gnome-extra/zeitgeist
	net-libs/webkit-gtk:3
"
DEPEND="${RDEPEND}
"
src_prepare(){
	DOCS="ChangeLog NEWS TODO AUTHORS"
	gnome2_src_prepare
}
