# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/gnome-extra/gnome-clocks/gnome-clocks-0.1.5.ebuild,v 1.1 2012/12/27 07:11:25 tetromino Exp $

EAPI="5"
GCONF_DEBUG="no"

inherit gnome2

DESCRIPTION="flickr applications for GNOME"
HOMEPAGE="https://live.gnome.org/Frogr"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+libsoupgnome"

RDEPEND="
	>=dev-libs/glib-2.32
	>=x11-libs/gtk+-3.4:3[introspection]
	media-libs/libexif
	dev-libs/libxml2
	media-libs/gstreamer:0.10
	libsoupgnome? ( net-libs/libsoupgnome:2.4 )
	!libsoupgnome? ( net-libs/libsoup:2.4 )
	dev-libs/libgcrypt
"
DEPEND="${RDEPEND}
"

src_prepare() {
	gnome2_src_prepare
}
