# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/gnome-extra/gnome-clocks/gnome-clocks-0.1.5.ebuild,v 1.1 2012/12/27 07:11:25 tetromino Exp $

EAPI="5"
GCONF_DEBUG="no"
VALA_MIN_API_VERSION="0.14"
VALA_USE_DEPEND="vapigen"
inherit gnome2 vala

DESCRIPTION="Cabinet files creator for GNOME"
HOMEPAGE="http://live.gnome.org/gcab"

LICENSE="GPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="doc introspection vala"

RDEPEND="
	>=dev-libs/glib-2.22:2
	introspection? ( >=dev-libs/gobject-introspection-0.9.4 )
	sys-libs/zlib
"
DEPEND="${RDEPEND}
	>=dev-util/gtk-doc-am-1.14
	vala? ( $(vala_depend) )
"
src_prepare(){
	gnome2_src_prepare
	use vala && vala_src_prepare
}
src_configure(){
	G2CONF="${G2CONF}
		$(use_enable doc gtk-doc)"
	if use introspection ;then
		G2CONF="${G2CONF} --enable-introspection=yes"
	else
		G2CONF="${G2CONF} --enable-introspection=no"
	fi

	gnome2_src_configure
}
