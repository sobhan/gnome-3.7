# Copyright 1999-2012 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: /var/cvsroot/gentoo-x86/gnome-extra/gnome-clocks/gnome-clocks-0.1.5.ebuild,v 1.1 2012/12/27 07:11:25 tetromino Exp $

EAPI="5"
GCONF_DEBUG="no"

inherit gnome2

DESCRIPTION="A versatile IDE for GNOME (extra)"
HOMEPAGE="http://www.anjuta.org"

LICENSE="GPL-2+"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

RDEPEND="
>=dev-util/anjuta-${PV}
>=x11-libs/gtk+-3:3
>=dev-libs/glib-2.16:2

"
DEPEND="${RDEPEND}
"
